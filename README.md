# Welcome to template of MVC client with back-end

Features

- Authentication, authorization by passport js
- express, express session
- Identity database model with claims/type resources (postgresql)
- logging (log4js)
- error handling
- handelbars and handelbars helpers as semantic templates
- require-namespace for smart imports
- uikit as css framework

How install
 
 - npm install
 - create database in postgres (sql script in repository)


TODO

 - Test api resource
 - Add gmail and telegram authentication
 - Add maintenance endpoint
 - Registration
 - profile info panel

