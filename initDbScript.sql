/*==============================================================*/
/* Table: accout                                                */
/*==============================================================*/
create table accout (
   accout_id            SERIAL               not null,
   password_hash        VARCHAR(512)         not null,
   password_salt        VARCHAR(512)         not null,
   username             VARCHAR(512)         not null,
   email                VARCHAR(512)         not null,
   constraint PK_ACCOUT primary key (accout_id)
);

/*==============================================================*/
/* Index: user_PK                                               */
/*==============================================================*/
create unique index user_PK on accout (
accout_id
);

/*==============================================================*/
/* Table: claim                                                 */
/*==============================================================*/
create table claim (
   claim_id             VARCHAR(36)          not null,
   resource_name        VARCHAR(20)          null,
   user_id              INT4                 null,
   name                 VARCHAR(20)          not null,
   value                VARCHAR(200)         not null,
   constraint PK_CLAIM primary key (claim_id)
);

/*==============================================================*/
/* Index: claim_PK                                              */
/*==============================================================*/
create unique index claim_PK on claim (
claim_id
);

/*==============================================================*/
/* Table: type_resource                                         */
/*==============================================================*/
create table type_resource (
   resource_name        VARCHAR(20)          not null,
   constraint PK_TYPE_RESOURCE primary key (resource_name)
);

alter table claim
   add constraint FK_CLAIM_REFERENCE_TYPE_RES foreign key (resource_name)
      references type_resource (resource_name)
      on delete restrict on update restrict;

alter table claim
   add constraint FK_CLAIM_REFERENCE_ACCOUT foreign key (user_id)
      references accout (accout_id)
      on delete cascade on update restrict;

