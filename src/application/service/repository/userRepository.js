const namespace = require('require-namespace')
let RepositoryPsql = namespace.infrastructure.repositoryPsql

class UserRepository extends RepositoryPsql {
  constructor () {
    super('account')
  }

  async getUserByName (name) {
    var account = await this.findOne({ username: name })
    if (!account) {
      return null
    }
    return account
  }
}

module.exports = new UserRepository()
