module.exports = class Claim {
  constructor (claimId, userId, resourceType, value) {
    this.claimId = claimId
    this.userId = userId
    this.resourceType = resourceType
    this.value = value
  }

  toString () {
    return `${this.resourceType}:${this.value}`
  }
}
