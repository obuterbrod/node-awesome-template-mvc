var config = require('config')
var promise = require('bluebird')
const namespace = require('require-namespace')
var errorHandler = namespace.utils.errorHandler

var options = {
  promiseLib: promise // overriding the default (ES6 Promise);
}

var pgp = require('pg-promise')(options)

module.exports = {
  init: function (connectionString) {
    pgp.end()
    try {
      let dbConfig = config.get('psqlDbConfig')
      this._db = pgp(dbConfig)
    } catch (error) {
      error.critical = true
      errorHandler(error)
    }
  },
  get: function () {
    return this._db
  }
}
