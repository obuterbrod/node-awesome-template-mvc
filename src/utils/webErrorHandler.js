module.exports = function (error, req, res, next) {
  res.render('error', {err: error.message, layout: 'error'})
  res.end()
}
